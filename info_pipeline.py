import http.client as cli
import json
import cfg
import os
import sys
from datetime import datetime

def check_exists_file(file_name):
	if os.path.exists(file_name):
		os.remove(file_name)

def create_file(file_name, type):
	f = open(file_name, 'w')
	f.write(f'id;iid;project_id;ref;status;started_at;finished_at;duration;web_url\n')

	return f

def write_file(f, j):
	dict_pipeline_ = {}

	for i in range(len(j)):
		dict_pipeline_ = {
			'id': j[i]['id'],
			'iid': j[i]['iid'],
			'project_id': j[i]['project_id'],
			'ref': j[i]['ref'],
			'status': j[i]['status'],
			'started_at': datetime.strptime(j[i]['started_at'], "%Y-%m-%dT%H:%M:%S.%f%z"),
			'finished_at': datetime.strptime(j[i]['finished_at'], "%Y-%m-%dT%H:%M:%S.%f%z"),
			'duration': j[i]['duration'],
			'web_url': j[i]['web_url']
		}

		f.write( f'{dict_pipeline_["id"]};{dict_pipeline_["iid"]};{dict_pipeline_["project_id"]};{dict_pipeline_["ref"]};{dict_pipeline_["status"]};{dict_pipeline_["started_at"]};{dict_pipeline_["finished_at"]};{dict_pipeline_["duration"]};{dict_pipeline_["web_url"]}\n')

def http_conn(url):
	c = cli.HTTPSConnection(url)
	c.close()

	return c

def get_info_pipeline(conn, headers, dict_projects_):
	text_single_pipeline = []
	for r in range(len(dict_projects_)):
		prj = dict_projects_[r]
		conn.request("GET", f"/api/v4/projects/{prj}/pipelines", None, headers)
		body = conn.getresponse().read()
		text = json.loads(body.decode('utf-8'))
		
		for i in range(len(text)):
			id = text[i]['id']
			conn.request("GET", f"/api/v4/projects/{prj}/pipelines/{id}", None, headers)
			body_single_pipeline = conn.getresponse().read()
			text_single_pipeline.append(json.loads(body_single_pipeline.decode('utf-8')))

	return text_single_pipeline


def create_csv():
	if os.environ.get('TOKEN') == None:
		print('None token')
		sys.exit()

	u = cfg.url
	headers = {'content-type': 'application/json', 'PRIVATE-TOKEN': os.environ.pop('TOKEN')}

	conn = http_conn(u)

	dict_projects_ = [
		'sashailyichev00%2Ftodoproject'
		,'petprject%2Fdata_test_tools'
	]
	j = []
	j = get_info_pipeline(conn, headers, dict_projects_)

	file_name = cfg.file_name
	check_exists_file(file_name)
	f = create_file(file_name, 'w')
	write_file(f, j)

if __name__ == '__main__':
	create_csv()